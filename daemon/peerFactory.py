import random
import time

from twisted.internet import protocol, reactor
from twisted.internet import task
from twisted.internet.threads import deferToThread

from daemon.peerProtocol import BasePeerProtocol, IncomingPeerProtocol, OutgoingPeerProtocol
from daemon.piece import Piece
from daemon.config import MAX_PROCESSING_PIECES, MAX_SERVING_BLOCKS, CHOKE_COOLDOWN, UNCHOKE_PERIOD, \
    PEER_RECONNECT_INTERVAL


__author__ = 'Edheltur'


class OutgoingPeerFactory(protocol.ClientFactory):
    """
    Класс, управляющий пирами. По одному экземпляру на каждый экземпляр Torrent
    torrent.peer_manager = PeerFactory
    """

    protocol = OutgoingPeerProtocol

    def buildProtocol(self, addr):
        peer_protocol = self.protocol(self.torrent, self.torrent.bitfield)
        peer_protocol.factory = self
        # self.torrent.peers[(addr.host, addr.port)] = peer_protocol
        return peer_protocol

    def __init__(self, torrent):
        self.max_processing_pieces = MAX_PROCESSING_PIECES
        self.last_choke = time.time()
        self.torrent = torrent
        self.processing_pieces = {}
        self.sending_queue = {}
        self.manage_peers()
        self.isGreedy = False
        self.last_piece_num = len(self.torrent.bitfield) - 1
        piece_len = int(self.torrent.meta_info[b'info'][b'piece length'])
        total_len = 0
        if b'files' in self.torrent.meta_info[b'info']:  # Multiple File Mode
            for file in self.torrent.meta_info[b'info'][b'files']:
                total_len += int(file[b'length'])
        else:  # Single File Mode
            total_len = int(self.torrent.meta_info[b'info'][b'length'])
        self.last_piece_len = total_len - self.last_piece_num * piece_len
        assert 0 <= self.last_piece_len <= piece_len
        task.LoopingCall(self.rethink).start(0.1)
        task.LoopingCall(self.find_fine_peers).start(5)
        task.LoopingCall(self.unchoke_some).start(UNCHOKE_PERIOD)

    def piece_done(self, number):
        if number in self.processing_pieces:
            current_piece = self.processing_pieces[number]
            offset = number * 20
            if current_piece.get_hash() == self.torrent.meta_info[b'info'][b'pieces'][offset:offset + 20]:
                print('Скачан', str(number) + 'й кусок, так и запишем...')
                self.torrent.file_stream.write(number, current_piece.data)
                self.torrent.bitfield[number] = 1
            else:
                print('Плохо скачан кусок', str(number) + ' давай заново')
            self.processing_pieces.pop(number)
            for peer in self.get_set_of_peers():
                if peer.peer_bitfield[number] == 0:
                    reactor.callLater(0, peer.have, number)

    def get_set_of_peers(self) -> list:
        peer_set = set()
        for peer in self.torrent.peers.values():
            if isinstance(peer, BasePeerProtocol):
                peer_set.add(peer)
        return peer_set

    def unchoke_some(self):
        if time.time() - self.last_choke > CHOKE_COOLDOWN:
            for peer in self.get_set_of_peers():
                if peer.peer_interested and peer.self_choking:
                    peer.unchoke()

    def choke_some(self):
        if time.time() - self.last_choke > CHOKE_COOLDOWN:
            self.last_choke = time.time()
            peer_set = self.get_set_of_peers()
            for peer in random.sample(peer_set, round(len(peer_set) / 5)):
                peer.choke()

    def queue_request(self, index, begin, length, peer: BasePeerProtocol):
        self.sending_queue[(index, begin, length, peer)] = None
        if len(self.sending_queue) > MAX_SERVING_BLOCKS:
            reactor.callLater(0, self.choke_some)

        def send_piece(block):
            self.sending_queue.pop((index, begin, length, peer))
            peer.piece(index, begin, block)

        def add_to_queue():
            if (index, begin, length, peer) in self.sending_queue:
                self.sending_queue[(index, begin, length, peer)] = reactor.callLater(0, send_piece)

        deferToThread(self.torrent.file_stream.read, index, begin, length).addCallback(add_to_queue)

    def clientConnectionLost(self, connector, reason):
        print('Lost  ', connector.host, connector.port, reason.value)
        self.torrent.peers[(connector.host, connector.port)] = 'Disconnected'
        reactor.callLater(0, self.torrent.peers.pop, (connector.host, connector.port))

    def clientConnectionFailed(self, connector, reason):
        print('Failed', connector.host, connector.port, reason.value)
        if hasattr(reason.value, 'osError') and reason.value.osError == 60:
            def reconnect():
                self.torrent.peers[(connector.host, connector.port)] = reactor.connectTCP(connector.host, connector.port, self, timeout=120)
            reactor.callLater(PEER_RECONNECT_INTERVAL, reconnect)
        else:
            self.torrent.peers[(connector.host, connector.port)] = 'Failed'
            reactor.callLater(0, self.torrent.peers.pop, (connector.host, connector.port))

    def state_changed(self, peer):
        # print(peer, 'State Changed')
        # проверим, есть ли нужные части у данного пира
        for piece_number in self.processing_pieces:
            if self.isGreedy or peer.peer_bitfield[piece_number] == 1:
                if not peer.self_interested:
                    # peer.unchoke()
                    peer.interested()
                # обновим список подходящих пиров, поскольку
                if not peer.peer_choking:
                    self.processing_pieces[piece_number].fine_peers.add(peer)
            else:
                self.processing_pieces[piece_number].fine_peers.discard(peer)
            if peer.peer_choking:
                self.processing_pieces[piece_number].fine_peers.discard(peer)

    def choke_all(self):
        for peer in self.get_set_of_peers():
            reactor.callLater(0, peer.choke)

    def manage_peers(self):
        for addr in self.torrent.peers:
            if self.torrent.peers[addr] == 'New':
                def connect(address):
                    self.torrent.peers[address] = reactor.connectTCP(address[0], address[1], self, timeout=120)

                reactor.callLater(0, connect, addr)

    def find_fine_peers(self):
        for peer in self.get_set_of_peers():
            for piece_num in self.processing_pieces:
                if not peer.peer_choking:
                    if self.isGreedy or peer.peer_bitfield[piece_num] == 1:
                        self.processing_pieces[piece_num].fine_peers.add(peer)

    def rethink(self):
        piece_length = self.torrent.meta_info[b'info'][b'piece length']

        for i in range(len(self.torrent.bitfield) - 1):
            if len(self.processing_pieces) < self.max_processing_pieces:
                if self.torrent.bitfield[i] == 0:
                    if i not in self.processing_pieces:
                        self.processing_pieces[i] = Piece(piece_length, i, self.piece_done)
            else:
                break

        if self.torrent.bitfield[self.last_piece_num] == 0:
            if len(self.processing_pieces) < self.max_processing_pieces:
                if self.last_piece_num not in self.processing_pieces:
                    self.processing_pieces[self.last_piece_num] = Piece(self.last_piece_len, self.last_piece_num,
                                                                        self.piece_done)

        for piece_num in self.processing_pieces:
            if len(self.processing_pieces[piece_num].fine_peers) != 0:
                reactor.callLater(0, self.processing_pieces[piece_num].ask)


class IncomingPeerFactory(protocol.ServerFactory):
    protocol = IncomingPeerProtocol
    unclassified_peers = {}

    def __init__(self, torrents, port):
        assert isinstance(torrents, list)
        self.torrents = torrents
        reactor.listenTCP(port, self)

    def buildProtocol(self, addr):
        print("Incoming connection", addr.host, addr.port)
        peer_protocol = self.protocol()
        peer_protocol.factory = self
        self.unclassified_peers[(addr.host, addr.port)] = peer_protocol
        return peer_protocol


    clientConnectionLost = OutgoingPeerFactory.clientConnectionLost
    clientConnectionFailed = OutgoingPeerFactory.clientConnectionFailed