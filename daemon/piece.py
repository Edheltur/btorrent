import hashlib
import random

from twisted.internet import reactor
from daemon.bitfield import Bitfield
from daemon.config import BLOCK_SIZE, MAX_PROCESSING_BLOCKS, BLOCK_REQUEST_TIMEOUT

__author__ = 'Edheltur'


class Piece:
    def decrement_processing_blocks(self):
        self.processing_blocks -= 1

    def request_block(self, block_index, peer):
        self.processing_blocks += 1
        reactor.callLater(BLOCK_REQUEST_TIMEOUT, self.decrement_processing_blocks)
        if self.length - block_index * BLOCK_SIZE < BLOCK_SIZE:
            size = self.length - block_index * BLOCK_SIZE
        else:
            size = BLOCK_SIZE
        reactor.callLater(0, peer.request, self.number, block_index * BLOCK_SIZE, size)

    def ask(self):
        for i in range(len(self.bitfield)):
            if self.processing_blocks >= MAX_PROCESSING_BLOCKS:
                break
            if self.bitfield[i] == 0:
                self.request_block(i, random.sample(self.fine_peers, 1)[0])

    def __init__(self, length, number, done_callback):
        self.number = number
        self.length = length
        self._done_callback = done_callback
        self.data = b'\x00' * length
        self.bitfield = Bitfield(length // BLOCK_SIZE if length % BLOCK_SIZE == 0 else length // BLOCK_SIZE + 1)
        self.fine_peers = set()
        self.processing_blocks = 0

    def write(self, begin, block):
        self.data = self.data[:begin] + block + self.data[begin + BLOCK_SIZE:]
        self.bitfield[begin // BLOCK_SIZE] = 1
        percent = self.bitfield.percent()
        print('BLOCK', percent, self.number)
        if percent == 100:
            reactor.callLater(0, self._done_callback, self.number)

    def get_hash(self):
        info_hash = hashlib.sha1()
        info_hash.update(self.data)
        return info_hash.digest()
