import os
import sys
from daemon.config import TORRENT_PORT
from daemon.peerProtocol import BasePeerProtocol

from twisted.internet import reactor
from twisted.internet import task

from daemon.bencode import LoadError, Load
from daemon.torrent import add_Torrent
from daemon.peerFactory import IncomingPeerFactory
from twisted.internet.base import BaseConnector


__author__ = 'Edheltur'


class Commands:
    """
    В данном классе реализованы реакции на команды пользователя.
    На каждую новую сессию создается новый экземпляр Commands.
    """
    user = None
    # Словарь torrents общий для всех экземпляров Commands
    torrents = {}
    incoming_peers = IncomingPeerFactory(list(torrents.values()), TORRENT_PORT)
    verbose = False

    def __init__(self, factory):
        self.user = factory
        self.list_task = task.LoopingCall(self.list_)

    def help(self):
        self.user.sendLine(b'Help page\n'
                           b'.----------------------------------------------------------------------------.\n'
                           b'|add <path>                 Adds a download using torrent file, specified in |\n'
                           b'|                           the <path>                                       |\n'
                           b'|list [start <period>]      Displays the list of downloads. With an argument |\n'
                           b'|                           "start", cyclically updated information after a  |\n'
                           b'|                           specified period of time until ^C+Enter          |\n'
                           b'|max priority <name>        Maximizes priority of the download specified in  |\n'
                           b'|                           the <name>                                       |\n'
                           b'|verbose <on/off>           Displays log information into your console       |\n'
                           b'|files                      Displays files for each download                 |\n'
                           b'|trackers                   Displays trackers for each download              |\n'
                           b'|peers                      Displays peers for each download                 |\n'
                           b'|pause <name>               Pauses download specified in the <name>          |\n'
                           b'|resume <name>              Resumes paused download specified in the <name>  |\n'
                           b'|exit                       Disconnects from the server                      |\n'
                           b'|halt                       Halts the server                                 |\n'
                           b'|help                       Displays this page                               |\n'
                           b"'----------------------------------------------------------------------------'")

    def toggle_verbose(self, mode):
        """
        Переключает флаг, отвечающий за дублирование вывода логов на консоль
        """
        if mode == b'on':
            self.verbose = True
            self.user.sendLine(b'Verbose is now ON')
        elif mode == b'off':
            self.verbose = False
            self.user.sendLine(b'Verbose is now OFF')
        else:
            self.user.sendLine(b'Wrong argument! Use "on" or "off" instead.')

    def pause(self, name):
        if name not in self.torrents:
            self.user.sendLine(b'Wrong name!')
            return
        if self.torrents[name].is_paused:
            self.user.sendLine(b'Already paused!')
            return
        self.torrents[name].pause()

    def resume(self, name):
        if name not in self.torrents:
            self.user.sendLine(b'Wrong name!')
            return
        if not self.torrents[name].is_paused:
            self.user.sendLine(b"Can't resume active torrent! ")
            return
        self.torrents[name].resume()

    def add(self, path):

        def is_valid():
            if os.path.exists(path):
                if os.path.isfile(path):
                    return True
                else:
                    self.user.sendLine(b'That is not a file!')
                    return False
            else:
                self.user.sendLine(b'File not exist!')
                return False

        if is_valid():
            try:
                torrent_file = Load(path.decode())
            except LoadError:
                self.user.sendLine(b'Corrupted .torrent file!')
            else:
                name = torrent_file.decoded_data[b'info'][b'name']
                meta_info = torrent_file.decoded_data
                info_hash = torrent_file.info_hash
                if meta_info in list(self.torrents[key].meta_info for key in self.torrents.keys()):
                    self.user.sendLine(b'Already added!')
                else:
                    # Далее идет генерация уникального имени
                    list_of_names = list(self.torrents.keys())
                    if name in list_of_names:
                        new_name = name + b'[1]'
                        number = 0
                        while new_name in list_of_names:
                            number += 1
                            new_name = name + b'[' + str(number).encode() + b']'
                        name = new_name
                    # Статический метод new_Torrent создает в отдельном потоке объект класса Torrent
                    # и добавляет его к списку всех торрентов(self._torrents)
                    add_Torrent(meta_info, info_hash, name, self.torrents, self.user.sendLine)

    def exit(self):
        self.user.sendLine(b'Goodbye!')
        self.user.transport.loseConnection()

    def halt(self):
        """
        Корректно завершает все соединения и попытки соединений,
        после чего останавливает сервер
        """
        def stop():
            for torrent_entry in self.torrents.values():
                for peer in torrent_entry.peers.values():
                    if isinstance(peer, BasePeerProtocol):
                        peer.transport.loseConnection()
                    elif isinstance(peer, BaseConnector):
                        peer.disconnect()
            for admin in self.user.factory.connected_admins:
                admin.control.exit()
            sys.stderr.close()
            sys.stdin.close()

            reactor.callLater(1, reactor.stop)

        self.user.sendLine(b'Stopping in 5 seconds...')
        for torrent in self.torrents.values():
            torrent.pause()
        reactor.callLater(4, stop)

    def max_priority(self, name):
        """
        Увеличивает приоритет (количество обрабатываемых кусков) одной раздачи,
        засчет уменьшения количества обрабатываемых кусков в других.
        """
        if name not in self.torrents:
            self.user.sendLine(b'Wrong name!')
            return
        free_pieces = 0
        for torrent in self.torrents.values():
            if torrent is not self.torrents[name]:
                free_pieces += torrent.peer_factory.max_processing_pieces - 1
                torrent.peer_factory.max_processing_pieces = 1
        self.torrents[name].peer_factory.max_processing_pieces += free_pieces

    def list_(self):
        self.user.sendLine(b'\n' * 50)
        for name in list(self.torrents):
            torrent = self.torrents[name]
            percent = torrent.bitfield.percent()
            bar_size = round(percent / 4)
            s = '▕{0}▏{1}% '.format('█' * bar_size + ' ' * (25 - bar_size), torrent.bitfield.percent())
            down_speed = str(int(torrent.current_download_speed / 128)).encode() + b' Kb/s '
            self.user.sendLine(s.encode() + down_speed + name)

    def list_start(self, interval):
        """
        Начинает циклично обновлять информацию о текущих загрузках
        """
        try:
            self.list_task.start(int(interval.decode()))
        except ValueError:
            self.user.sendLine(b'Invalid argument - ' + interval)

    def list_stop(self):
        self.list_task.stop()

    def files(self):
        self.user.sendLine(b"|FILES:")
        for torrent in list(self.torrents.values()):
            self.user.sendLine(b"|" + torrent.name)
            for file in torrent.file_stream.files:
                self.user.sendLine(b"|--" + file[0].encode())

    def trackers_info(self):
        self.user.sendLine(b"|TRACKERS INFO:")
        for torrent in list(self.torrents.values()):
            self.user.sendLine(b"|" + torrent.name)
            for tracker in torrent.trackers:
                self.user.sendLine(b"|--" + tracker.get_info().encode())

    def peers_info(self):
        self.user.sendLine(b"|PEERS INFO:")
        for torrent in list(self.torrents.values()):
            self.user.sendLine(b"|" + torrent.name + b' ' + str(torrent.peers).encode())

    def unknown(self):
        self.user.sendLine(b'Wrong command!')