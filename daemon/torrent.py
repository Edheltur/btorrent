import random
from string import digits, ascii_letters

from twisted.internet import task
from twisted.internet import reactor
from twisted.internet.threads import deferToThread
from twisted.internet.defer import inlineCallbacks
from twisted.names.error import DomainError

from daemon.config import NUMWANT, TORRENT_PORT, MAX_PROCESSING_PIECES
from daemon.trackerFactory import TrackerFactory, AnnounceError
from daemon.version import __version__ as VERSION
from daemon.fileStream import FileStream
from daemon.bitfield import Bitfield
from daemon.peerFactory import OutgoingPeerFactory

__author__ = 'Edheltur'


class TorrentError(Exception):
    pass


class Torrent:
    meta_info = None
    name = ''

    info_hash = b''
    peer_id = b''
    port = 0
    uploaded = 0
    downloaded = 0
    left = 0
    event = b'started'
    compact = b'1'
    no_peer_id = b'1'
    num_want = NUMWANT

    file_stream = None
    peer_factory = None

    def __init__(self, name, meta_info, info_hash, port=TORRENT_PORT):
        self.trackers = []
        self.is_paused = False
        self.peers = {}
        self.current_download_speed = 0
        self.current_upload_speed = 0
        self.downloaded_last_sec = 0
        self.uploaded_last_sec = 0
        task.LoopingCall(self.measure_speed).start(1)
        self.name = name
        self.meta_info = meta_info
        self.info_hash = info_hash
        self.port = port
        real_size = len(self.meta_info[b'info'][b'pieces']) // 20
        spare = 8 - real_size % 8
        self.bitfield = Bitfield(real_size + spare, spare)
        ver = str(10000 + int(VERSION[:VERSION.find('.')]) * 100 + int(VERSION[VERSION.find('.') + 1:]))[1:].encode()
        self.peer_id = b'-bT' + ver + b'-' + ''.join(random.choice(ascii_letters + digits) for i in range(12)).encode()

    def measure_speed(self):
        self.uploaded += self.uploaded_last_sec
        self.current_upload_speed = (self.uploaded_last_sec + self.current_upload_speed) // 2
        self.uploaded_last_sec = 0

        self.downloaded += self.downloaded_last_sec
        self.current_download_speed = (self.downloaded_last_sec + self.current_download_speed) // 2
        self.downloaded_last_sec = 0

    def pause(self):
        self.event = b'stopped'
        self.is_paused = True
        for tracker in self.trackers:
            reactor.callLater(0, tracker.stop_announcing)
        if isinstance(self.peer_factory, OutgoingPeerFactory):
            self.peer_factory.choke_all()
            self.peer_factory.max_processing_pieces = 0

    def resume(self):
        if self.bitfield.percent() == 100:
            self.event = b'completed'
        else:
            self.event = b'started'
        for tracker in self.trackers:
            reactor.callLater(0, tracker.start_announcing)
        if isinstance(self.peer_factory, OutgoingPeerFactory):
            self.peer_factory.choke_all()
            self.peer_factory.max_processing_pieces = MAX_PROCESSING_PIECES


@inlineCallbacks
def add_Torrent(meta_info: dict, info_hash, name, torrents_list: dict, print_to_user):
    new_torrent = yield deferToThread(Torrent, name, meta_info, info_hash)

    torrents_list[name] = new_torrent

    print_to_user(name + b' added')

    new_torrent.file_stream = yield deferToThread(FileStream, meta_info)

    tracker_factory = yield deferToThread(TrackerFactory, new_torrent)

    if b'announce-list' in new_torrent.meta_info:
        announce_list = new_torrent.meta_info[b'announce-list']
    else:
        announce_list = [[new_torrent.meta_info[b'announce']]]
    for announce in announce_list:
        try:
            tracker = yield tracker_factory.build_protocol(announce[0])
        except AnnounceError as e:
            print("Announce Error", e)
        except DomainError as e:
            print("Unknown host", e.args[0].queries[0].name)
        else:
            tracker.start_announcing()
            new_torrent.trackers.append(tracker)

    new_torrent.peer_factory = yield deferToThread(OutgoingPeerFactory, new_torrent)
