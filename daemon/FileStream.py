__author__ = 'Edheltur'
import itertools


def create_empty_file(path, length):
    # 256 MB
    chunk_size = 2 ** 25
    left = length
    with open(path, 'w+b') as f:
        while left > 0:
            if left - chunk_size > 0:
                current_size = chunk_size
                left -= current_size
            else:
                current_size = left
                left -= current_size
            f.write(b'\x00' * current_size)
            f.flush()


class FileStream:
    def __init__(self, meta_info):
        self.meta_info = meta_info
        self.files = []
        if b'files' in meta_info[b'info']:  # Multiple File Mode
            for file in meta_info[b'info'][b'files']:
                path = file[b'path'][0].decode()
                length = int(file[b'length'])
                create_empty_file(path, length)
                self.files.append([path, length, 0])
        else:  # Single File Mode
            path = meta_info[b'info'][b'name'].decode()
            length = int(meta_info[b'info'][b'length'])
            create_empty_file(path, length)
            self.files.append([path, length, 0])
            # path, length, left


    def write(self, index, piece):
        assert self.meta_info[b'info'][b'piece length'] >= len(piece)
        buffer = piece
        cur_pos = index * len(piece)
        i = 0
        total_bound = 0
        while len(buffer) > 0:
            total_bound += self.files[i][1]
            if cur_pos < total_bound:
                offset = cur_pos - (total_bound - self.files[i][1])
                capacity = total_bound - cur_pos

                with open(self.files[i][0], 'r+b') as f:
                    f.seek(offset)
                    if len(buffer) <= capacity:
                        self.files[i][2] += f.write(buffer)
                        buffer = b''
                    else:
                        self.files[i][2] += f.write(buffer[:capacity])
                        buffer = buffer[capacity:]
                        cur_pos += capacity
                    f.close()
            else:
                i += 1

    def read(self, index, begin, length):
        total_offset = self.meta_info[b'info'][b'piece length'] * index + begin
        total_pos = 0
        for file in self.files:
            if file[1] + total_pos > total_offset:
                result = b''
                bytes_left = length
                local_offset = total_offset - total_pos
                # начинаем итерацию с места, до которого дошли во внешнем цикле
                for fil in itertools.dropwhile(lambda x: x is not file, self.files):
                    f = open(fil[0], 'rb')
                    f.seek(local_offset)
                    bytes_to_EOF = fil[1] - local_offset
                    if bytes_left > bytes_to_EOF:
                        result += f.read(fil[1])
                        bytes_left -= fil[1]
                        f.close()
                        local_offset = 0
                    else:
                        result += f.read(bytes_left)
                        f.close()
                        break
                return result
            else:
                total_pos += file[1]