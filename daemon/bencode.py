import hashlib


class LoadError(Exception):
    pass


class IntConvertError(LoadError):
    def __init__(self):
        pass


class UnexpectedChar(LoadError):
    def __init__(self, position):
        self.pos = position
    def __str__(self):
        return repr(self.pos)


class Load:
    _position = 0
    _data = b''
    info_hash = b''
    _info_start = 0
    _info_end = 0

    def __init__(self, source):
        self.decoded_data = {}
        if type(source) is str:
            with open(source, 'rb') as file:
                self._data = file.read()
                file.close()
                self.decoded_data = self._choose_type()
                info_hash = hashlib.sha1()
                info_value = self._data[self._info_start:self._info_end]
                info_hash.update(info_value)
                self.info_hash = info_hash.digest()
        elif type(source) is bytes:
            self._data = source
            self.decoded_data = self._decode_dict()

    def _parse_int(self, escape):
        integer = ''
        while self._data[self._position] != escape:
            integer += chr(self._data[self._position])
            self._position += 1
        self._position += 1
        try:
            integer = int(integer)
        except ValueError:
            raise IntConvertError()

        return integer

    def _decode_int(self):
        self._position += 1
        integer = self._parse_int(ord('e'))
        return integer

    def _decode_string(self):
        length = self._parse_int(ord(':'))
        string = self._data[self._position: self._position + length]
        self._position += length
        return string

    def _choose_type(self):
        cur_char = self._data[self._position]
        if cur_char == ord('i'):
            return self._decode_int()
        # (b'0', b'1', b'2', b'3', b'4', b'5', b'6', b'7', b'8', b'9'):
        elif cur_char in range(48, 58):
            return self._decode_string()
        elif cur_char == ord('l'):
            return self._decode_list()
        elif cur_char == ord('d'):
            return self._decode_dict()
        else:
            raise UnexpectedChar(self._position)

    def _decode_list(self):
        self._position += 1
        list_ = []
        while self._data[self._position] != 101:
            list_.append(self._choose_type())
        self._position += 1
        return list_

    def _decode_dict(self):
        in_info = False
        self._position += 1
        dict_ = {}
        while self._data[self._position] != 101:
            key = self._decode_string()
            if key == b'info':
                self._info_start = self._position
                in_info = True
            value = self._choose_type()
            if in_info:
                self._info_end = self._position
                in_info = False
            dict_[key] = value
        self._position += 1
        return dict_
