__author__ = 'Edheltur'
import unittest
import random
import string

import sys

sys.path = ['.', ] + sys.path  # Позволяет импортировать и родительской директории

from daemon.bencode import Load


class LoadTestCase(unittest.TestCase):
    class TestLoad(Load):
        def __init__(self, source):
            pass

    def setUp(self):
        self.test = self.TestLoad('')

    def test_parse_int(self):
        integer = random.randint(-10 ** 10, 10 ** 10)
        escape = random.choice(':e')
        self.test._data = str(integer).encode() + escape.encode()
        result = self.test._parse_int(ord(escape))
        self.assertTrue(result == int(self.test._data[:-1]),
                        'Wrong convertation: ' + self.test._data.decode() + ' != ' + str(result))


    def test_decode_int(self):
        integer = random.randint(-10 ** 10, 10 ** 10)
        self.test._data = b'i' + str(integer).encode() + b'e'
        result = self.test._decode_int()
        self.assertTrue(result == integer, 'Wrong convertation: ' + str(integer) + ' != ' + str(result))

    def test_decode_string(self):
        length = random.randint(0, 20)
        s = ''.join(random.choice(string.ascii_letters + string.digits) for i in range(length))
        self.test._data = str(length).encode() + b':' + s.encode()
        result = self.test._decode_string()
        self.assertTrue(result == s.encode(), 'Wrong convertation: ' + s + ' != ' + str(result))

    def test_all(self):
        for i in range(5):
            print(Load('test' + str(i) + '.torrent').decoded_data[b'info'][b'name'])


    def test_decode_dict(self):
        self.test._data = b'd4:testi456e3:fooli678ei123eee'
        result = self.test._decode_dict()
        self.assertTrue(result == {b'test': 456, b'foo': [678, 123]}, 'Wrong parsing')

    def test_decode_list(self):
        self.test._data = b'li123ei456e3:foo3:bare'
        result = self.test._decode_list()
        self.assertTrue(result == [123, 456, b'foo', b'bar'], 'Wrong parsing')

    def test_choose_type(self):
        self.test._data = b'5:abcde3:123i12345eli987eli654eei-321eei999999999ed3:onei11e3:twoi22ee4:bye!'
        while self.test._position != len(self.test._data):
            result = self.test._choose_type()
            print(result)


if __name__ == '__main__':
    unittest.main()