__author__ = 'Edheltur'
import unittest

import sys

sys.path = ['.', ] + sys.path  # Позволяет импортировать и родительской директории

from daemon.bitfield import Bitfield


class LoadTestCase(unittest.TestCase):
    def setUp(self):
        pass

    def test_from_to_bytes(self):
        a = Bitfield(len(b'Edheltur') * 8)
        a.from_bytes(b'Edheltur')
        self.assertTrue(a.to_bytes() == b'Edheltur', 'Wrong convertation: ' + a.to_bytes().decode() + ' != Edheltur')


if __name__ == '__main__':
    unittest.main()