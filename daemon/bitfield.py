__author__ = 'Edheltur'


class Bitfield:
    def __init__(self, size=0, spare=0):
        self.int = 0
        self.spare = spare
        self.total_size = size

    def __repr__(self):
        representation = ''
        for i in range(len(self)):
            representation += '1' if self[i] == 1 else '0'
        return representation + '.' + '0' * self.spare

    def __getitem__(self, i):
        return (self.int >> (self.total_size - i - 1)) % 2

    def __setitem__(self, i, y):
        if self[i] != y:
            self.int ^= 2 ** (self.total_size - i - 1)

    def __len__(self):
        return self.total_size - self.spare

    def to_bytes(self):
        size = self.total_size // 8
        if self.total_size % 8 != 0:
            size += 1
        return self.int.to_bytes(size, byteorder='big')

    def from_bytes(self, bytes_):
        self.int = int.from_bytes(bytes_, byteorder='big')

    def percent(self):
        return round(100 * (bin(self.int).count('1')) / (self.total_size - self.spare), 2)
