# Patches twisted to force returning IPv4 address on dns lookup
import socket

from twisted.names import dns
from twisted.names import common


def myExtractRecord(resolver, name, answers, level=10):
    if not level:
        return None
    for r in answers:
        if r.name == name and r.type == dns.A:
            return socket.inet_ntop(socket.AF_INET, r.payload.address)
    for r in answers:
        if r.name == name and r.type == dns.CNAME:
            result = myExtractRecord(
                resolver, r.payload.name, answers, level - 1)
            if not result:
                return resolver.getHostByName(
                    str(r.payload.name), effort=level - 1)
            return result
    # No answers, but maybe there's a hint at who we should be asking about
    # this
    for r in answers:
        if r.type == dns.NS:
            from twisted.names import client

            r = client.Resolver(servers=[(str(r.payload.name), dns.PORT)])
            return r.lookupAddress(str(name)
            ).addCallback(
                lambda records: myExtractRecord(
                    r, name,
                    records[_ANS] + records[_AUTH] + records[_ADD],
                    level - 1))

common.extractRecord = myExtractRecord
