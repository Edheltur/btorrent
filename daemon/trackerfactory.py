__author__ = 'Edheltur'
import os
import re
import time
from urllib.parse import quote

from twisted.internet import reactor
from twisted.internet import protocol
from twisted.internet.threads import deferToThread
from twisted.web.client import getPage
from daemon.bencode import Load


class AnnounceError(Exception):
    pass


class EmptyResponseError(AnnounceError):
    pass


class UnsupportedProtocolError(AnnounceError):
    pass


class HTTPProtocol():
    torrent = None
    announce_address = b''
    interval = 800
    min_interval = 800
    seeders = 0
    leechers = 0
    tracker_id = b''

    announce_task = None
    is_last_announce = False
    error = "None"

    def __init__(self, torrent, announce):
        self.torrent = torrent
        self.announce_address = announce
        self.last_announce = time.time()

    def stop_announcing(self):
        if self.announce_task is not None:
            self.announce_task.cancel()
        self.is_last_announce = True
        reactor.callLater(0, self.announce)

    def start_announcing(self):
        self.is_last_announce = False
        reactor.callLater(0, self.announce)

    def get_info(self):
        next_announce = str(int(self.last_announce + self.interval - time.time()))
        info = self.announce_address.decode() + " next announce in " + next_announce + "s"
        info += " errors: " + self.error
        return info

    def announce_received(self, tracker_response):
        if tracker_response is None:
            raise EmptyResponseError("Response is Empty")
        elif b'failure reason' in tracker_response.decoded_data:
            raise AnnounceError(tracker_response.decoded_data[b'failure reason'].decode())
        else:
            self.interval = tracker_response.decoded_data.get(b'min interval', self.interval)
            self.min_interval = tracker_response.decoded_data.get(b'interval', self.min_interval)
            self.tracker_id = tracker_response.decoded_data.get(b'tracker id', self.tracker_id)
            self.seeders = tracker_response.decoded_data.get(b'complete', self.seeders)
            self.leechers = tracker_response.decoded_data.get(b'incomplete', self.leechers)
            raw_peers = tracker_response.decoded_data.get(b'peers')
            if type(raw_peers) is bytes:
                for peer in (raw_peers[i:i + 6] for i in range(0, len(raw_peers), 6)):
                    address = '.'.join((str(peer[0]), str(peer[1]), str(peer[2]), str(peer[3])))
                    port = int.from_bytes(peer[-2:], byteorder='big')
                    if (address, port) not in self.torrent.peers:
                        self.torrent.peers[(address, port)] = 'New'
            elif type(raw_peers) is dict:
                raise NotImplementedError
            self.last_announce = time.time()
            if not self.is_last_announce:
                self.announce_task = reactor.callLater(self.interval, self.announce)
            if self.torrent.peer_factory is not None:
                reactor.callLater(0, self.torrent.peer_factory.manage_peers)
            print("New HTTP Announce:", self.torrent.name, self.interval, self.torrent.peers)

    def announce(self):

        args = [b'info_hash=' + quote(self.torrent.info_hash).encode(),
                b'peer_id=' + self.torrent.peer_id,
                b'port=' + str(self.torrent.port).encode(),
                b'uploaded=' + str(self.torrent.uploaded).encode(),
                b'downloaded=' + str(self.torrent.downloaded).encode(),
                b'left=' + str(self.torrent.left).encode(),
                b'numwant=' + str(self.torrent.num_want).encode(),
                b'event=' + self.torrent.event,
                b'compact=' + self.torrent.compact,
                b'no_peer_id=' + self.torrent.no_peer_id]
        request = self.announce_address + b'?' + b'&'.join(args)

        def error_handler(failure):
            # e.trap(AnnounceError, DomainError)
            self.error = str(failure.value)
            if self.announce_address != b"http://retracker.local/announce":
                print("Error in " + self.announce_address.decode(), self.error)

        def load_failed(failure):
            self.error = str(failure.value)
            print("Error response is not bencode:", failure.value)

        raw_tracker_response_d = getPage(request)
        raw_tracker_response_d.addCallbacks(Load, error_handler)
        raw_tracker_response_d.addCallbacks(self.announce_received, load_failed)
        raw_tracker_response_d.addErrback(error_handler)


class UDPProtocol(protocol.DatagramProtocol):
    interval = 800
    min_interval = 800
    seeders = 0
    leechers = 0
    tracker_id = b''
    last_announce = time.time()

    announce_task = None
    is_last_announce = False
    error = None

    def __init__(self, host, port, torrent):
        self.host = host
        self.port = port
        self.torrent = torrent
        self.transactions = {}

    def stop_announcing(self):
        if self.announce_task is not None:
            self.announce_task.cancel()
        self.is_last_announce = True
        reactor.callLater(0, self.do, self.announce)

    def start_announcing(self):
        self.is_last_announce = False
        reactor.callLater(0, self.do, self.announce)

    def get_info(self):
        next_announce = str(int(self.last_announce + self.interval - time.time()))
        info = "udp://" + self.host + " next announce in " + next_announce + "s"
        info += " errors: " + self.error
        return info

    def startProtocol(self):
        self.transport.connect(self.host, self.port)

    def do(self, action):
        def retry():
            if transaction_id in self.transactions:
                del self.transactions[transaction_id]
                self.do(action)

        transaction_id = os.urandom(4)
        self.transactions[transaction_id] = action
        reactor.callLater(5, retry)
        datagram = b'\x00\x00\x04\x17\x27\x10\x19\x80\x00\x00\x00\x00' + transaction_id
        self.transport.write(datagram)
        # print('    Send:', datagram)

    def scrape(self, connection_id):
        """
        scrape input
        Offset	    Size	        Name	        Value
        0	        64-bit integer	connection_id
        8	        32-bit integer	action	        2
        12	        32-bit integer	transaction_id
        16 + 20 * n	20-byte string	info_hash
        16 + 20 * N
        """
        transaction_id = os.urandom(4)
        self.transactions[transaction_id] = None
        datagram = connection_id
        datagram += b'\x00\x00\x00\x02'  # action
        datagram += transaction_id
        datagram += self.torrent.info_hash
        self.transport.write(datagram)
        # TODO Доделать multi-scrape

    def scrape_received(self, datagram):
        """
        scrape output
        Offset	    Size	        Name	        Value
        0	        32-bit integer	action	        2
        4	        32-bit integer	transaction_id
        8 + 12 * n	32-bit integer	seeders
        12 + 12 * n	32-bit integer	completed
        16 + 12 * n	32-bit integer	leechers
        8 + 12 * N
        """
        pass

    # TODO: Реализовать scrape

    def announce(self, connection_id):
        """
        announce input
        Offset	Size	        Name	        Value
        0	    64-bit integer	connection_id
        8	    32-bit integer	action	        1
        12	32-bit integer	    transaction_id
        16	20-byte string	    info_hash
        36	20-byte string	    peer_id
        56	64-bit integer  	downloaded
        64	64-bit integer	    left
        72	64-bit integer	    uploaded
        80	32-bit integer	    event
        84	32-bit integer	    IP address	    0
        88	32-bit integer	    key
        92	32-bit integer	    num_want	    -1
        96	16-bit integer	    port
        98
        """
        transaction_id = os.urandom(4)
        self.transactions[transaction_id] = None
        datagram = connection_id
        datagram += b'\x00\x00\x00\x01'  # action
        datagram += transaction_id
        datagram += self.torrent.info_hash
        datagram += self.torrent.peer_id
        datagram += self.torrent.downloaded.to_bytes(8, byteorder='big')
        datagram += self.torrent.left.to_bytes(8, byteorder='big')
        datagram += self.torrent.uploaded.to_bytes(8, byteorder='big')
        if self.torrent.event == b'none':
            datagram += b'\x00\x00\x00\x00'
        elif self.torrent.event == b'completed':
            datagram += b'\x00\x00\x00\x01'
        elif self.torrent.event == b'started':
            datagram += b'\x00\x00\x00\x02'
        elif self.torrent.event == b'stopped':
            datagram += b'\x00\x00\x00\x03'
        datagram += b'\x00\x00\x00\x00'  # ip
        datagram += os.urandom(4)  # key
        datagram += self.torrent.num_want.to_bytes(4, byteorder='big')
        datagram += self.torrent.port.to_bytes(4, byteorder='big')
        self.transport.write(datagram)

    def announce_received(self, datagram):
        """
        Offset	    Size            Name	        Value
        0	        32-bit integer	action	        1
        4	        32-bit integer	transaction_id
        8	        32-bit integer	interval
        12	        32-bit integer	leechers
        16	        32-bit integer	seeders
        20 + 6 * n	32-bit integer	IP address
        24 + 6 * n	16-bit integer	TCP port
        20 + 6 * N
        """
        self.interval = max(int.from_bytes(datagram[8:12], byteorder='big'), 10)
        self.leechers = int.from_bytes(datagram[12:16], byteorder='big')
        self.seeders = int.from_bytes(datagram[16:20], byteorder='big')
        raw_peers = datagram[20:]
        for peer in (raw_peers[i:i + 6] for i in range(0, len(raw_peers), 6)):
            address = '.'.join((str(peer[0]), str(peer[1]), str(peer[2]), str(peer[3])))
            port = int.from_bytes(peer[-2:], byteorder='big')
            if port == 0:
                break
            if (address, port) not in self.torrent.peers:
                self.torrent.peers[(address, port)] = 'New'
        self.last_announce = time.time()
        if not self.is_last_announce:
            self.announce_task = reactor.callLater(self.interval, self.do, self.announce)

        if self.torrent.peer_factory is not None:
            reactor.callLater(0, self.torrent.peer_factory.manage_peers)
        print("New UDP Announce:",
              list(self.torrent.peers.keys()))  # self.torrent.name, self.interval, self.torrent.peers)

    def datagramReceived(self, datagram, addr):
        try:
            transaction_id = datagram[4:8]
            if transaction_id in self.transactions:
                action = int.from_bytes(datagram[:4], byteorder='big')
                if action == 0:
                    self.transactions[transaction_id](datagram[8:16])
                elif action == 1:
                    if len(datagram) >= 20:
                        self.announce_received(datagram)
                        reactor.callLater(self.interval, self.do, self.announce)
                    else:
                        raise EmptyResponseError("Response is Empty")
                elif action == 2:
                    pass
                elif action == 3:
                    pass
                else:
                    print('Unknown action')
            self.transactions.pop(transaction_id)
        except AnnounceError as e:
            self.error = str(e)
            print("Error in " + self.host, self.error)


class TrackerFactory():
    torrent = None

    def __init__(self, torrent):
        self.torrent = torrent

    def build_protocol(self, announce):
        hostname = announce[announce.find(b':') + 3:announce.rfind(b':')]
        if announce.startswith(b'udp://'):
            a = re.match(r"udp://(?P<hostname>[\w\.]+):(?P<port>\d+)", announce.decode())
            port = int(a.group('port'))

            def resolve_succeed(ip):
                udp_protocol = UDPProtocol(ip, port, self.torrent)
                reactor.listenUDP(0, udp_protocol)
                return udp_protocol

            def is_ip(host_name):
                octets = host_name.split('.')
                if len(octets) == 4:
                    for octet in octets:
                        if octet.isnumeric():
                            if not 0 <= int(octet) <= 255:
                                return False
                        else:
                            return False
                else:
                    return False
                return True

            if is_ip(hostname.decode()):
                return deferToThread(resolve_succeed, hostname.decode())
            else:
                return reactor.resolve(hostname.decode()).addCallback(resolve_succeed)
        elif announce.startswith(b'http://') or announce.startswith(b'https://'):
            return HTTPProtocol(self.torrent, announce)
        else:
            raise UnsupportedProtocolError(announce)
