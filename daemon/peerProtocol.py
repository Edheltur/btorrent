from daemon.bitfield import Bitfield
from twisted.internet import protocol, reactor, task
from twisted.internet.error import AlreadyCancelled, AlreadyCalled

__author__ = 'Edheltur'


class BasePeerProtocol(protocol.Protocol):
    """
    Класс, реализующий API обмена данными и отправки сообщений с определенным пиром.
    torrent.peers{(address, port):Peer}
    """
    factory = None

    def __repr__(self, *args, **kwargs):
        s = str(self.peer_bitfield.percent()) + "%"
        if self.peer_choking:
            s += ' is Choking'
        else:
            s += ' not Choking'
        return s

    def __init__(self):
        self.drop_connection_callID = None
        self.handshake_complete = False
        self._message_buffer = b''
        self._message_len = 0
        self._in_message = False
        self.self_choking = True
        self.peer_choking = True
        self.self_interested = False
        self.peer_interested = False

    def connectionMade(self):
        self.drop_connection_callID = reactor.callLater(125, self.transport.loseConnection)

    def finish_init(self, torrent, bitfield: Bitfield):
        self.torrent = torrent
        self.self_bitfield = bitfield
        self.peer_bitfield = Bitfield(self.torrent.bitfield.total_size, self.torrent.bitfield.spare)

    def send_handshake(self, pstr=b'BitTorrent protocol', reserved=b'\x00\x00\x00\x00\x00\x00\x00\x00'):
        self.transport.write(chr(len(pstr)).encode() +
                             pstr +
                             reserved +
                             self.torrent.info_hash +
                             self.torrent.peer_id)

    def keep_alive(self):
        self.transport.write(b'\x00\x00\x00\x00')

    def choke(self):
        self.self_choking = True
        self.transport.write(b'\x00\x00\x00\x01' + b'\x00')

    def unchoke(self):
        self.self_choking = False
        self.transport.write(b'\x00\x00\x00\x01' + b'\x01')

    def interested(self):
        self.self_interested = True
        self.transport.write(b'\x00\x00\x00\x01' + b'\x02')

    def not_interested(self):
        self.self_interested = False
        self.transport.write(b'\x00\x00\x00\x01' + b'\x03')

    def have(self, piece_index):
        self.transport.write(b'\x00\x00\x00\x05' + b'\x04' + piece_index.to_bytes(4, byteorder='big'))

    def bitfield(self):
        bitfield = self.self_bitfield.to_bytes()
        self.transport.write((len(bitfield) + 1).to_bytes(4, byteorder='big') + b'\x05' + bitfield)

    def request(self, index, begin, length):
        # print('Requested', index, begin, length)
        self.transport.write(b'\x00\x00\x00\x0D' +
                             b'\x06' +
                             index.to_bytes(4, byteorder='big') +
                             begin.to_bytes(4, byteorder='big') +
                             length.to_bytes(4, byteorder='big'))

    def _request_received(self, payload):
        # <index><begin><length>
        index = int.from_bytes(payload[0:4], byteorder='big')
        begin = int.from_bytes(payload[4:8], byteorder='big')
        length = int.from_bytes(payload[8:12], byteorder='big')
        self.factory.queue_request(index, begin, length, self)

    def piece(self, index, begin, block):
        length = 9 + len(block)
        self.transport.write(length.to_bytes(4, byteorder='big') +
                             b'\x07' +
                             index.to_bytes(4, byteorder='big') +
                             begin.to_bytes(4, byteorder='big') +
                             block)

    def _piece_received(self, payload):
        # <index><begin><block>
        index = int.from_bytes(payload[0:4], byteorder='big')
        begin = int.from_bytes(payload[4:8], byteorder='big')
        block = payload[8:]

        if index in self.factory.processing_pieces:
            self.factory.processing_pieces[index].write(begin, block)
            self.torrent.downloaded_last_sec += len(block)

    def cancel(self, index, begin, length):
        self.transport.write(b'\x00\x00\x00\x0D' +
                             b'\x08' +
                             index.to_bytes(4, byteorder='big') +
                             begin.to_bytes(4, byteorder='big') +
                             length.to_bytes(4, byteorder='big'))

    def _cancel_received(self, payload):
        # <index><begin><length>
        index = int.from_bytes(payload[0:4], byteorder='big')
        begin = int.from_bytes(payload[4:8], byteorder='big')
        length = int.from_bytes(payload[8:12], byteorder='big')
        self.factory.sending_queue[(index, begin, length, self)].cancel()
        self.factory.sending_queue.pop((index, begin, length, self))

    def port(self, listen_port):
        self.transport.write(b'\x00\x00\x00\x03' +
                             b'\x09' +
                             listen_port.to_bytes(2, byteorder='big'))

    def message_received(self, data):
        try:
            self.drop_connection_callID.cancel()
            self.drop_connection_callID = reactor.callLater(180, self.transport.loseConnection)
        except AlreadyCancelled:
            pass
        except AlreadyCalled:
            pass
        message_id = data[:1]
        payload = data[1:]
        if message_id == b'':  # keep alive
            pass
        elif message_id == b'\x00':  # choke
            self.peer_choking = True
        elif message_id == b'\x01':  # unchoke
            self.peer_choking = False
        elif message_id == b'\x02':  # interested
            self.peer_interested = True
        elif message_id == b'\x03':  # not interested
            self.peer_interested = False
        elif message_id == b'\x04':  # have
            self.peer_bitfield[int.from_bytes(payload, byteorder='big')] = 1
        elif message_id == b'\x05':  # bitfield
            self.peer_bitfield.from_bytes(payload)
        elif message_id == b'\x06':  # request
            self._request_received(payload)
        elif message_id == b'\x07':  # piece
            self._piece_received(payload)
        elif message_id == b'\x08':  # cancel
            pass
        elif message_id == b'\x09':  # port
            pass
        else:
            print('Unknown message', payload)

        if message_id != b'':
            reactor.callLater(0, self.factory.state_changed, self)

    def finish_handshake(self, data):
        pass

    def dataReceived(self, data):
        """
        Буферизирует данные, разбитые на несколько пакетов, и вызывает
        метод message_received, передавая в качестве аргумента сообщение
        без префикса длины
        """
        self._message_buffer += data
        if not self.handshake_complete:
            pstrlen = self._message_buffer[0]
            if len(self._message_buffer) >= 49 + pstrlen:
                reactor.callLater(0, self.finish_handshake, self._message_buffer[:49 + pstrlen])
                self._message_buffer = self._message_buffer[49 + pstrlen:]
            else:
                return

        while True:
            if self._in_message:
                if len(self._message_buffer) >= self._message_len:
                    reactor.callLater(0, self.message_received, self._message_buffer[:self._message_len])
                    self._message_buffer = self._message_buffer[self._message_len:]
                    self._in_message = False
                else:
                    return
            else:
                if len(self._message_buffer) >= 4:
                    self._message_len = int.from_bytes(self._message_buffer[:4], byteorder='big')
                    self._message_buffer = self._message_buffer[4:]
                    self._in_message = True
                else:
                    return


class IncomingPeerProtocol(BasePeerProtocol):
    def __init__(self):
        super().__init__()

    def finish_handshake(self, data):
        host = self.transport.getPeer().host
        port = self.transport.getPeer().port

        pstrlen = data[0]
        if data[1:pstrlen + 1] == b'BitTorrent protocol':
            for torrent in self.factory.torrents:
                if data[pstrlen + 9:pstrlen + 29] == torrent.info_hash:
                    self.finish_init(torrent, torrent.bitfield)
                    self.factory = torrent.peer_factory
                    self.handshake_complete = True
                    if self.self_bitfield.percent() != 0:
                        self.bitfield()

                    reactor.callLater(0, self.send_handshake)
                    self.torrent.peers[(host, port)] = self.factory.unclassified_peers.pop((host, port))
                    task.LoopingCall(self.keep_alive).start(60)
                    self.factory = self.torrent.peer_factory
                    break
            else:
                print('Wrong info_hash', data[pstrlen + 9:pstrlen + 29])
                self.transport.loseConnection()
                self.factory.unclassified_peers.pop((host, port))
        else:
            print('Wrong handshake start')
            self.transport.loseConnection()
            self.factory.unclassified_peers.pop((host, port))


class OutgoingPeerProtocol(BasePeerProtocol):
    def __init__(self, torrent, bitfield: Bitfield):
        super().__init__()
        self.finish_init(torrent, bitfield)

    def connectionMade(self):
        super().connectionMade()
        reactor.callLater(0, self.send_handshake)

    def finish_handshake(self, data):
        host = self.transport.getPeer().host
        port = self.transport.getPeer().port

        pstrlen = data[0]
        if data[1:pstrlen + 1] == b'BitTorrent protocol' and data[pstrlen + 9:pstrlen + 29] == self.torrent.info_hash:
            self.handshake_complete = True
            if self.self_bitfield.percent() != 0:
                self.bitfield()
            self.torrent.peers[(host, port)] = self
            task.LoopingCall(self.keep_alive).start(60)
        else:
            self.transport.loseConnection()
            self.torrent.peers[(host, port)] = 'Disconnected'