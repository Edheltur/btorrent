import sys

from twisted.protocols.basic import LineReceiver
from twisted.internet import protocol
from twisted.internet import reactor
from twisted.names import client

import daemon.dnspatch
from daemon.config import LOG_FILE_NAME, ERR_FILE_NAME, MANAGEMENT_CONSOLE_PORT
from daemon.version import __version__ as VERSION
from daemon.commands import Commands

__author__ = 'Edheltur'

if __name__ == '__main__':
    WHITELIST = ['127.0.0.1']

class Admin(LineReceiver):
    control = None
    factory = None

    def __init__(self):
        pass

    def connectionMade(self):
        address = self.transport.getPeer().host
        if address in WHITELIST:
            self.control = Commands(self)
            self.factory.connected_admins.append(self)
            self.sendLine(
                b'Welcome to bTorrent Daemon version ' + VERSION.encode() + b'. Your IP is ' + address.encode())
        else:
            self.transport.loseConnection()

    def connectionLost(self, reason=None):
        self.factory.connected_admins.remove(self)


    def lineReceived(self, line):
        if line == b'help':
            self.control.help()
        elif line[:4] == b'add ':
            self.control.add(line[4:])
        elif line == b'halt':
            self.control.halt()
        elif line == b'peers':
            self.control.peers_info()
        elif line == b'trackers':
            self.control.trackers_info()
        elif line == b'exit':
            self.control.exit()
        elif line == b'list':
            self.control.list_()
        elif line[:10] == b'list start':
            self.control.list_start(line[11:])
        elif line == b'^C':
            self.control.list_stop()
        elif line == b'files':
            self.control.files()
        elif line[:6] == b'pause ':
            self.control.pause(line[6:])
        elif line[:7] == b'resume ':
            self.control.resume(line[7:])
        elif line[:13] == b'max priority ':
            self.control.max_priority(line[13:])
        elif line[:8] == b'verbose ':
            self.control.toggle_verbose(line[8:])
        elif line == b'\x1b[A':
            self.lineReceived(b'add /Users/kanaroo/PycharmProjects/bTorrent/hp.torrent')
        elif line == b'\x1b[B':
            self.lineReceived(b'add /Users/kanaroo/PycharmProjects/bTorrent/aria.torrent')
        elif line == b'\x1b[D':
            self.lineReceived(b'add /Users/kanaroo/PycharmProjects/bTorrent/scorp.torrent')
        elif line == b'\x1b[C':
            self.lineReceived(b'add /Users/kanaroo/PycharmProjects/bTorrent/is.torrent')
        else:
            self.control.unknown()


class AdminFactory(protocol.ServerFactory):
    connected_admins = []
    def buildProtocol(self, addr):
        admin = Admin()
        admin.factory = self
        return admin

if __name__ == '__main__':
    admin_factory = AdminFactory()

class Logger(object):
    def __init__(self, filename="Default.log"):
        self.terminal = sys.stdout
        self.log = open(filename, "a")

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)
        for admin in admin_factory.connected_admins:
            if admin.control.verbose is True:
                admin.transport.write(message.encode())

    def flush(self):
        self.terminal.flush()
        self.log.flush()

    def close(self):
        self.log.close()

if __name__ == '__main__':
    sys.stdout = Logger(LOG_FILE_NAME)
    sys.stderr = Logger(ERR_FILE_NAME)

    reactor.installResolver(client.createResolver(servers=[('8.8.8.8', 53), ('8.8.4.4', 53)]))
    reactor.listenTCP(MANAGEMENT_CONSOLE_PORT, admin_factory)
    reactor.run()
